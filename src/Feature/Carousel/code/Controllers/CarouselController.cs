﻿using akqa.Feature.Carousel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace akqa.Feature.Carousel.Controllers
{
    public class CarouselController : Controller
    {
        private readonly ICarouselService _carouselService;
        // GET: Carousel
        public CarouselController(ICarouselService CarouselService)
        {
            _carouselService = CarouselService;
        }
        public ActionResult Index()
        {
            var viewmodel = _carouselService.CreateViewModel();
            return View(viewmodel);
        }
    }
}